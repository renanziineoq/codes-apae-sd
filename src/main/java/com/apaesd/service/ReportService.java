package com.apaesd.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.apaesd.dto.AlunoDTO;
import com.apaesd.dto.ProdutoDTO;
import com.apaesd.dto.TriagemFonoaudiologicaDTO;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.apaesd.model.Aluno;
import com.apaesd.model.AvaliacaoServicoSocial;
import com.apaesd.model.Produto;
import com.apaesd.model.TriagemFonoaudiologica;
import com.apaesd.repository.AlunoRepository;
import com.apaesd.repository.AvaliacaoServicoSocialRepository;
import com.apaesd.repository.ProdutoRepository;
import com.apaesd.repository.TriagemFonoaudiologiaRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Service
public class ReportService {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private AlunoRepository alunoRepository;

    @Autowired
    private TriagemFonoaudiologiaRepository triagemFonoaudiologiaRepository;

    @Autowired
    private AvaliacaoServicoSocialRepository avaliacaoServicoSocialRepository;

    public String exportReportProduto() throws FileNotFoundException, JRException {
        final String pathDestino = System.getProperty("java.io.tmpdir") + File.pathSeparator+  System.currentTimeMillis() + ".pdf";
        final List<Produto> produtos = produtoRepository.findAll();
        final Map<String, Object> parameters = new HashMap<>();
        final List<ProdutoDTO> produtosDTO = new ArrayList<>();
        for (final Produto produto : produtos) {
            ProdutoDTO produtoDTO = new ProdutoDTO();
            produtoDTO.setId(produto.getId());
            produtoDTO.setDescricao(produto.getDescricao());
            produtoDTO.setNomeProduto(produto.getNomeProduto());
            produtoDTO.setQuantidade(produto.getQuantidade());
            produtoDTO.setTipoProduto(produto.getTipoProduto());
            produtoDTO.setTipoQuantidade(produto.getTipoQuantidade());

            produtosDTO.add(produtoDTO);


        }

        final String path = "templates/reports/RelacaoProduto.jrxml";
        //load file and compile it
        final File file = new File(ReportService.class.getClassLoader().getResource(path).getFile());
        final JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        final JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(produtosDTO);

        final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        JasperExportManager.exportReportToPdfFile(jasperPrint, pathDestino);

        return pathDestino;
    }

    public String exportReportAluno() throws FileNotFoundException, JRException {
        final String pathDestino = System.getProperty("java.io.tmpdir") + File.pathSeparator+  System.currentTimeMillis() + ".pdf";
        final List<Aluno> alunos = alunoRepository.findAll();
        final Map<String, Object> parameters = new HashMap<>();

        final List<AlunoDTO> alunosDTO = new ArrayList<>();

        for (final Aluno aluno : alunos) {
           AlunoDTO alunoDTO = new AlunoDTO();
           alunoDTO.setId(aluno.getId());
           alunoDTO.setPai(aluno.getPai());
           alunoDTO.setMae(aluno.getMae());
           alunoDTO.setResponsavel(aluno.getResponsavel());
           alunoDTO.setDescricao(aluno.getDescricao());
           alunoDTO.setNome(aluno.getNome());
           alunoDTO.setIdade(aluno.getIdade());
           alunoDTO.setDataNascimento(aluno.getDataNascimento());
           alunoDTO.setUf(aluno.getUf());
           alunoDTO.setCidade(aluno.getCidade());
           alunoDTO.setRua(aluno.getRua());
           alunoDTO.setNumero(aluno.getNumero());
           alunoDTO.setBairro(aluno.getBairro());
           alunoDTO.setCep(aluno.getCep());
           alunoDTO.setTelefone(aluno.getTelefone());
           alunoDTO.setCelular(aluno.getCelular());
           alunoDTO.setStatus(aluno.getStatus());
           alunoDTO.setSexo(aluno.getSexo());
           alunosDTO.add(alunoDTO);
        }

        final String path = "templates/reports/RelatorioAluno.jrxml";
        //load file and compile it
        final File file = new File(ReportService.class.getClassLoader().getResource(path).getFile());
        final JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        final JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(alunosDTO);

        final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        JasperExportManager.exportReportToPdfFile(jasperPrint, pathDestino);

        return pathDestino;
    }

    public String exportReportTriagemFonoaudiologica() throws FileNotFoundException, JRException {

        final String pathDestino = System.getProperty("java.io.tmpdir") + File.pathSeparator+  System.currentTimeMillis() + ".pdf";

        final List<TriagemFonoaudiologica> triagensFonoaudiologica = triagemFonoaudiologiaRepository.findAll();

        final Map<String, Object> parameters = new HashMap<>();

        final List<TriagemFonoaudiologicaDTO> triagensFonoaudiologicaDTO = new ArrayList<>();

           for(final TriagemFonoaudiologica triagem : triagensFonoaudiologica){
               TriagemFonoaudiologicaDTO triagemFonoaudiologicaDTO = new TriagemFonoaudiologicaDTO();
               triagemFonoaudiologicaDTO.setNome(triagem.getAluno().getNome());
               triagemFonoaudiologicaDTO.setId(triagem.getId());
               triagemFonoaudiologicaDTO.setDataTriagem(triagem.getDataTriagem());
               triagemFonoaudiologicaDTO.setEscola(triagem.getEscola());
               triagemFonoaudiologicaDTO.setEncaminhado(triagem.getEncaminhado());
               triagemFonoaudiologicaDTO.setQueixa(triagem.getQueixa());
               triagemFonoaudiologicaDTO.setOfas(triagem.getOfas());
               triagemFonoaudiologicaDTO.setRespiracao(triagem.getRespiracao());
               triagemFonoaudiologicaDTO.setVoz(triagem.getVoz());
               triagemFonoaudiologicaDTO.setAudicao(triagem.getAudicao());
               triagemFonoaudiologicaDTO.setComunicacao(triagem.getComunicacao());
               triagemFonoaudiologicaDTO.setDisfluencia(triagem.getDisfluencia());
               triagemFonoaudiologicaDTO.setRealfa(triagem.getRealfa());
               triagemFonoaudiologicaDTO.setOmissoes(triagem.getOmissoes());
               triagemFonoaudiologicaDTO.setDistorcoes(triagem.getDistorcoes());
               triagemFonoaudiologicaDTO.setSubstituicoes(triagem.getSubstituicoes());
               triagemFonoaudiologicaDTO.setEncaminhado(triagem.getEncaminhado());
               triagemFonoaudiologicaDTO.setAtendimentoAudiologia(triagem.getAtendimentoAudiologia());
               triagemFonoaudiologicaDTO.setAtendimentoLinguagem(triagem.getAtendimentoLinguagem());
               triagemFonoaudiologicaDTO.setAtendimentoMotrocidadeOrofacial(triagem.getAtendimentoMotrocidadeOrofacial());
               triagemFonoaudiologicaDTO.setAtendimentoVoz(triagem.getAtendimentoVoz());

               triagensFonoaudiologicaDTO.add(triagemFonoaudiologicaDTO);



           }


        final String path = "templates/reports/RelatorioTriagemFonoaudiologia.jrxml";
        //load file and compile it
        final File file = new File(ReportService.class.getClassLoader().getResource(path).getFile());
        final JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        final JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(triagensFonoaudiologicaDTO);

        final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        JasperExportManager.exportReportToPdfFile(jasperPrint, pathDestino);

        return pathDestino;
    }

    public String exportReportAvaliacaoSocial() throws FileNotFoundException, JRException {

        final List<AvaliacaoServicoSocial> avaliacoesSociais = avaliacaoServicoSocialRepository.findAll();

        final Map<String, Object> parameters = new HashMap<>();

           for(final AvaliacaoServicoSocial avaliacaoSocial : avaliacoesSociais){
               parameters.put("encaminhadoPor", avaliacaoSocial.getEncaminhadoPor());
               parameters.put("dataEvolucao", avaliacaoSocial.getDataAvaliacao());
               parameters.put("identificacao", avaliacaoSocial.getIdentificacao());
               parameters.put("dataNascimento", avaliacaoSocial.getDataNascimento());
               parameters.put("documento", avaliacaoSocial.getDocumentoId());
               parameters.put("cpf", avaliacaoSocial.getCpf());
               parameters.put("pai", avaliacaoSocial.getPai());
               parameters.put("profissaoPai", avaliacaoSocial.getProfissaopai());
               parameters.put("mae", avaliacaoSocial.getMae());
               parameters.put("profissaoMae", avaliacaoSocial.getProfissaoMae());
               parameters.put("numeroIrmao", avaliacaoSocial.getNumeroIrmao());
               parameters.put("numeroIrma", avaliacaoSocial.getNumeroIrma());
               parameters.put("posicaoFamiliar", avaliacaoSocial.getPosicaoFamiliar());
               parameters.put("endereco", avaliacaoSocial.getEndereco());
               parameters.put("pontoReferencia", avaliacaoSocial.getPontoReferencia());
               parameters.put("responsavel", avaliacaoSocial.getResponsavel());
               parameters.put("beneficioSus", avaliacaoSocial.isBeneficioSus());
               parameters.put("beneficioBolsa", avaliacaoSocial.isBeneficioBolsa());
               parameters.put("telefone", avaliacaoSocial.getTelefone());
               parameters.put("nis", avaliacaoSocial.getNis());
               parameters.put("sus", avaliacaoSocial.getSus());
               parameters.put("aluno", avaliacaoSocial.getAluno());
               parameters.put("tipoAreaMoradia", avaliacaoSocial.getTipoAreaMoradia());
               parameters.put("tipoCasa", avaliacaoSocial.getTipoCasa());
               parameters.put("valorMoradia", avaliacaoSocial.getValorMoradia());
               parameters.put("numeroComodos", avaliacaoSocial.getNumeroComodos());
               parameters.put("agua", avaliacaoSocial.getAgua());
               parameters.put("destinoEsgoto", avaliacaoSocial.getDestinoEsgoto());
               parameters.put("energiaEletrica", avaliacaoSocial.getEnergiaEletrica());
               parameters.put("pavimentacao", avaliacaoSocial.getPavimentacao());
               parameters.put("lixo", avaliacaoSocial.getLixo());
               parameters.put("energiaValor", avaliacaoSocial.getEnergiaValor());
               parameters.put("aguaValor", avaliacaoSocial.getAguaValor());
               parameters.put("gasValor", avaliacaoSocial.getGasValor());
               parameters.put("alimentacaoValor", avaliacaoSocial.getAlimentacaoValor());
               parameters.put("transporte", avaliacaoSocial.getTransporte());
               parameters.put("aluguelValor", avaliacaoSocial.getAlguel_valor());
               parameters.put("medicamentoValor", avaliacaoSocial.getMedicamentoValor());
               parameters.put("meioTransporte", avaliacaoSocial.getMeioTransporte());
               parameters.put("tipoGravidez", avaliacaoSocial.getTipoGravidez());
               parameters.put("teveAborto", avaliacaoSocial.getTeveAborto());
               parameters.put("alimentacaoGravidez", avaliacaoSocial.getAlimentacaoGravidez());
               parameters.put("drogaAlcool", avaliacaoSocial.getUsoDrogaAlcool());
               parameters.put("qualDrogaAlcoo", avaliacaoSocial.getQualDrogaAlcoo());
               parameters.put("frequenciaDrogaAlcool", avaliacaoSocial.getFrequenciaDrogaAlcoo());
               parameters.put("realizouPreNatal", avaliacaoSocial.getRealizouPreNatal());
               parameters.put("usouRemedioGravidez", avaliacaoSocial.getUsouRemedioGravidez());
               parameters.put("doencaGestacao", avaliacaoSocial.getDoencaGestacao());
               parameters.put("qualDoencaGestacao", avaliacaoSocial.getQualDoencaGestacao());
               parameters.put("passouConturbacao", avaliacaoSocial.getPassouConturbacao());
               parameters.put("localParto", avaliacaoSocial.getLocalParto());
               parameters.put("outroLocalParto", avaliacaoSocial.getOutroLocalParto());
               parameters.put("tipoParto", avaliacaoSocial.getTipoParto());
               parameters.put("tempoParto", avaliacaoSocial.getTempoParto());
               parameters.put("chorouParto", avaliacaoSocial.getChorouParto());
               parameters.put("oxigenioParto", avaliacaoSocial.getOxigenioParto());
               parameters.put("getIncubadora", avaliacaoSocial.getIncubadora());
               parameters.put("sugouNascer", avaliacaoSocial.getSugouNascer());
               parameters.put("chupetaDedo", avaliacaoSocial.getChupetaDedo());
               parameters.put("engasgarFrequentemente", avaliacaoSocial.getEngasgaFrequentemente());
               parameters.put("alimentaBem", avaliacaoSocial.getAlimentaBem());
               parameters.put("sono", avaliacaoSocial.getSono());
               parameters.put("quartoPais", avaliacaoSocial.getQuartoPais());
               parameters.put("mesmaCama", avaliacaoSocial.getMesmaCama());
               parameters.put("controleEsfincteres", avaliacaoSocial.getControleEsfincteres());
               parameters.put("doencasQueTeve", avaliacaoSocial.getDoencasQueTeve());
               parameters.put("teveInternacao", avaliacaoSocial.getTeveInternacao());
               parameters.put("porqueTeveInternacao", avaliacaoSocial.getPorqueTeveInternacao());
               parameters.put("teveConvulsao", avaliacaoSocial.getTeveConvulsao());
               parameters.put("quantasTeveConvulsao", avaliacaoSocial.getQuantasTeveConvulsao());
               parameters.put("sofreuDesmaio", avaliacaoSocial.getSofreuDesmaio());
               parameters.put("ouveBem", avaliacaoSocial.getOuveBem());
               parameters.put("exameAuditivo", avaliacaoSocial.getExameAuditivo());
               parameters.put("tratamentoNeurologico", avaliacaoSocial.getTratamentoNeurologico());
               parameters.put("medicoTratamentoNeurologico", avaliacaoSocial.getMedicoTratamentoNeurologico());
               parameters.put("engatinhar", avaliacaoSocial.getEngatinhar());
               parameters.put("anosEngatinhar", avaliacaoSocial.getAnosEngatinhar());
               parameters.put("correSalta", avaliacaoSocial.getCorreSalta());
               parameters.put("caiFacilidade", avaliacaoSocial.getCaiFacilidade());
               parameters.put("problemaFala", avaliacaoSocial.getProblemaFala());
               parameters.put("compreendeOrdem", avaliacaoSocial.getCompreendeOrdem());
               parameters.put("eResponsavel", avaliacaoSocial.getEResponsavel());
               parameters.put("maisGostaFazer", avaliacaoSocial.getMaisGostaFazer());
               parameters.put("frequentaLocalPublico", avaliacaoSocial.getFrequentaLocalPublico());
               parameters.put("sozinhoAcompanhado", avaliacaoSocial.getSozinhoAcompanhado());
               parameters.put("contatoDeficiencia", avaliacaoSocial.getContatoDeficiencia());
               parameters.put("preconceitoDescriminacao", avaliacaoSocial.getPreconceitoDescriminacao());
               parameters.put("trabalhoCasa", avaliacaoSocial.getTranalhoCasa());
               parameters.put("conheceManipulaDinheiro", avaliacaoSocial.getConheceManipulaDinherio());
               parameters.put("opiniaoSolicitadaFamilia", avaliacaoSocial.getOpiniaoSolicitadaFamilia());
               parameters.put("eleitor", avaliacaoSocial.getEleitor());
               parameters.put("vota", avaliacaoSocial.getVota());
               parameters.put("clubeEsportivo", avaliacaoSocial.getClubeEsportivo());
               parameters.put("creas", avaliacaoSocial.getCreas());
               parameters.put("escola", avaliacaoSocial.getEscola());
               parameters.put("praca", avaliacaoSocial.getPraca());
               parameters.put("psf", avaliacaoSocial.getPsf());
               parameters.put("frequentaEscola", avaliacaoSocial.getCriancaFrequentaEscola());
               parameters.put("criancaFrequentaEscola", avaliacaoSocial.getQualCriancaFrequentaEscola());
               parameters.put("fezPrePrimario", avaliacaoSocial.getFezPrePrimario());
               parameters.put("adaptacaoEscola", avaliacaoSocial.getAdaptacaoEscola());
               parameters.put("gostaEscola", avaliacaoSocial.getGostaEscola());
               parameters.put("convivioColegas", avaliacaoSocial.getConvivioColegas());
               parameters.put("dificuldadesApresentadas", avaliacaoSocial.getDificuldadesApresentadas());
               parameters.put("outroDificuldadesApresentadas", avaliacaoSocial.getOutroDificuldadesApresentadas());
               parameters.put("comportamentoEscola", avaliacaoSocial.getComportamentoEscola());
               parameters.put("deficienciaApresenta", avaliacaoSocial.getDeficienciaApresenta());
               parameters.put("quandoDescobriuDeficiencia", avaliacaoSocial.getQuandoDescobriuDeficiencia());
               parameters.put("tipoExame", avaliacaoSocial.getTipoExame());
               parameters.put("aceitacaoFamiliares", avaliacaoSocial.getAceitacaoFamiliares());
               parameters.put("avaliacaoSocial", avaliacaoSocial.getFazTratamentoCom());
               parameters.put("fazUsoMedicacao", avaliacaoSocial.getFazUsoMedicacao());
               parameters.put("observacoes", avaliacaoSocial.getObservacoes());
           }


        final String path = "templates/reports/RelatorioAvaliacaoServicoSocial.jrxml";
        //load file and compile it
        final File file = new File(ReportService.class.getClassLoader().getResource(path).getFile());
        final JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        final JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(avaliacoesSociais);

        final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        JasperExportManager.exportReportToPdfFile(jasperPrint, "D:/Relatório de Avaliação de Serviço Social" + System.currentTimeMillis() + ".pdf");

        return "redirect:/triagemfonoaudiologia/lista";
    }


}


