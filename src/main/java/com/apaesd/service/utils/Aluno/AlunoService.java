package com.apaesd.service.utils.Aluno;

import com.apaesd.model.Aluno;


public interface AlunoService {
    void save(Aluno nome);

    Aluno findByNomeAluno(String nome);
}
