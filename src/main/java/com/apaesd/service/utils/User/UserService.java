package com.apaesd.service.utils.User;

import com.apaesd.model.Usuario;

public interface UserService {
    void save(Usuario user);

    Usuario findByUsername(String username);
}
