package com.apaesd.service.utils.User;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import com.apaesd.model.Permissao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.apaesd.model.Usuario;
import com.apaesd.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

    private UserRepository userRepository;

    public UserDetailsServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String nomeUsuario) {
        Usuario usuario = userRepository.findByNomeUsuario(nomeUsuario);
        System.out.println("xablablau!!!");
        if (usuario == null) throw new UsernameNotFoundException(nomeUsuario);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        final String permissao = Optional.ofNullable(usuario.getPermissao()).map(Permissao::getPermissao).orElse("USER");
        grantedAuthorities.add(new SimpleGrantedAuthority(permissao));

        return new org.springframework.security.core.userdetails.User(usuario.getNomeUsuario(), usuario.getSenha(), grantedAuthorities);
    }
}
