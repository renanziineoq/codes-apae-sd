package com.apaesd.service.utils.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.apaesd.model.Usuario;
import com.apaesd.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(Usuario user) {
        user.setSenha((bCryptPasswordEncoder.encode(user.getSenha())));
        Usuario.builder().senha("terte").email("terteyrt").build();
        userRepository.save(user);
    }

    @Override
    public Usuario findByUsername(String nomeUsuario) {
        return userRepository.findByNomeUsuario(nomeUsuario);
    }
}
