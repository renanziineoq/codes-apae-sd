package com.apaesd.model;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "avaliacao_fisioterapia")
@Data
public class AvaliacaoFisioterapia extends BasicModel{

    @Column(name = "expressao_facial")
    private String expressaoFacial;
    @Column(name = "observacao_expressao_facial")
    private String observacaoExpressaoFacial;
    @Column(name = "data_avaliacao")
    private Date dataAvaliacao;
    @Column(name = "atividade_reflexa")
    private String atividadeReflexa;
    @Column(name = "observacao_atividade_reflexa")
    private String observacaoAtividadeReflexa;
    @Column(name = "resposta_estimulacao_digital")
    private String respostaEstimulacaoDigital;
    @Column(name = "observacao_resposta_estimulacao_digital")
    private String observacaoRespostaEstimulacaoDigital;
    @Column(name = "obtencao_alimentos_sugar")
    private String obtencaoAlimentosSugar;
    @Column(name = "observacao_obtencao_alimentos_sugar")
    private String observacaoObtencaoAlimentosSugar;
    @Column(name = "obtencao_alimentos_beber")
    private String obtencaoAlimentosBeber;
    @Column(name = "observacao_obtencao_alimentos_beber")
    private String observacaoObtencaoAlimentosBeber;
    @Column(name = "obtencao_alimentos_colher")
    private String obtencaoAlimentosColher;
    @Column(name = "observacao_obtencao_alimentos_colher")
    private String observacaoObtencaoAlimentosColher;
    @Column(name = "obtencao_alimentos_canudo")
    private String obtencaoAlimentosCanudo;
    @Column(name = "observacao_obtencao_alimentos_canudo")
    private String observacaoObtencaoAlimentosCanudo;
    @Column(name = "obtencao_alimentos_deglutir")
    private String obtencaoAlimentosDeglutir;
    @Column(name = "observacao_obtencao_alimentos_deglutir")
    private String observacaoObtencaoAlimentosDeglutir;
    @Column(name = "obtencao_alimentos_mastigar")
    private String obtencaoAlimentosMastigar;
    @Column(name = "observacao_obtencao_alimentos_mastigar")
    private String observacaoObtencaoAlimentosMastigar;
    @Column(name = "obtencao_alimentos_posicionamento")
    private String obtencaoAlimentosPosicionamento;
    @Column(name = "observacao_obtencao_alimentos_posicionamento")
    private String observacaoObtencaoAlimentosPosicionamento;
    private String denticao;
    @Column(name = "observacao_denticao")
    private String observacaoDenticao;
    private String mandibula;
    @Column(name = "observacao_mandibula")
    private String observacaoManbidula;
    private String labios;
    @Column(name = "observacao_labios")
    private String observacaoLabios;
    private String lingua;
    @Column(name = "observacao_lingua")
    private String observacaoLingua;
    private String respiracao;
    @Column(name = "observacao_respiracao")
    private String observacaoRespiracao;
    private String voz;
    @Column(name = "observacao_voz")
    private String observacaoVoz;
    private String articulacao;
    private String comunicacao;
    @Column(name = "linguagem_compreensao_audibilidade")
    private String linguagemCompreensaoAudibilidade;
    private String conclusao;
    @Column(name = "procedimentos_recomendados")
    private String procedimentosRecomendados;
    @Column(name = "pontos_positivos")
    private String pontosPositivos;
    @ManyToOne
    @JoinColumn(name = "id_aluno")
    private Aluno aluno;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;


}
