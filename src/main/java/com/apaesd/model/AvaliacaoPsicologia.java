package com.apaesd.model;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "avaliacao_psicologia")
@Data
public class AvaliacaoPsicologia extends BasicModel{

    @Column(name = "motivo_encaminhamento")
    private String motivoEncaminhamento;
    @Column(name = "data_anamnese")
    private Date dataAnamnese;
    private String concepcao;
    private String parto;
    @Column(name = "condicao_nascimento")
    private String condicaoNascimento;
    private String engatinhar;
    private String andar;
    private String falar;
    private String alimentacao;
    @Column(name = "reconhecimento_cor")
    private String reconhecimentoCor;
    @Column(name = "identifica_formas")
    private String identificaFormas;
    @Column(name = "movimenta_som")
    private String movimentaSom;
    @Column(name = "identifica_som_baixo")
    private String identificaSomBaixo;
    @Column(name = "identifica_som_alto")
    private String identificaSomAlto;
    @Column(name = "percebe_temperatura")
    private String percebeTemperatura;
    @Column(name = "interesse_estudo")
    private String interesseEstudo;
    @Column(name = "contribui_ideias")
    private String contribuiIdeias;
    @Column(name = "interessa_colega")
    private String interessaColega;
    @Column(name = "participa_pergunta")
    private String participaPergunta;
    @Column(name = "encontrar_resposta")
    private String encontrarResposta;
    @Column(name = "participa_interesse")
    private String participaInteresse;
    @Column(name = "dispensa_atividade")
    private String dispensaAtividade;
    @Column(name = "facilidade_andar")
    private String facilidadeAndar;
    private String correr;
    private String pular;
    private String saltar;
    private String rolar;
    @Column(name = "participa_ativamente")
    private String participaAtivamente;
    @Column(name = "consegue_relaxamento")
    private String consegueRelaxamento;
    private String construir;
    private String empilhar;
    private String enfiar;
    private String dobrar;
    private String calcar;
    private String modelar;
    private String colar;
    private String desenhar;
    private String pintar;
    private String picar;
    private String recortar;
    @Column(name = "persiste_concluir")
    private String persisteConcluir;
    @ManyToOne
    @JoinColumn(name = "id_aluno")
    private Aluno aluno;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
}
