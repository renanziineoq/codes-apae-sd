package com.apaesd.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "triagem_fonoaudiologica")
@Data
public class TriagemFonoaudiologica extends BasicModel{

    @ManyToOne
    @JoinColumn(name = "id_aluno")
    private Aluno aluno;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    @Column(name = "data_triagem")
    private Date dataTriagem;
    private String escola;
    private String encaminhado;
    private String queixa;
    private String ofas;
    private String respiracao;
    private String voz;
    private String audicao;
    private String comunicacao;
    private String disfluencia;
    private String realfa;
    private String omissoes;
    private String distorcoes;
    private String substituicoes;
    private String encaminhamentos;
    @Column(name = "atendimento_audiologia")
    private String atendimentoAudiologia;
    @Column(name = "atendimento_linguagem")
    private String atendimentoLinguagem;
    @Column(name = "atendimento_motrocidade_orafacial")
    private String atendimentoMotrocidadeOrofacial;
    @Column(name = "atendimento_voz")
    private String atendimentoVoz;

}
