package com.apaesd.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author renan
 * @since 09/09/2019
 */
@Entity
@Table(name = "linha_semanal")
@Data
public class LinhaSemanal extends BasicModel {

    @Column(name = "data_linha_semanal")
    private Date dataLinhaSemanal;
    private String descricao;
    @ManyToOne
    @JoinColumn(name = "id_aluno")
    private Aluno aluno;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;


}
