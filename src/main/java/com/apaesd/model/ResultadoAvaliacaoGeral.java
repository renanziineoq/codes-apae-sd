package com.apaesd.model;

import lombok.Data;

import java.util.Date;

import javax.persistence.*;

/**
 * @author Rodrigo Moliterno
 * @since 16/11/2019
 */
@Entity
@Table(name = "resultado_avaliacao_geral")
@Data
public class ResultadoAvaliacaoGeral extends BasicModel {
    @ManyToOne
    @JoinColumn(name = "id_aluno")
    private Aluno aluno;

    @Column(name = "data_avaliacao")
    private Date dataAvaliacao;

    @Column(name = "motivo")
    private String motivo;

    @Column(name = "setor_fisioterapia")
    private String setorFisioterapia;

    @Column(name = "setor_fonoaudiologia")
    private String setorFonoaudiologia;

    @Column(name = "setor_psicologia")
    private String setorPsicologia;

    @Column(name = "setor_terapia_ocupacional")
    private String setorTerapiaOcupacional;

    @Column(name = "resultado_avaliacao")
    private String resultadoAvaliacao;

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    
}