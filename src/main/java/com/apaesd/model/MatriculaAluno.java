package com.apaesd.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "matricula_aluno")
@Data
public class MatriculaAluno extends BasicModel {

    @Column(name = "nivel_escolaridade")
    private String nivelEscolaridade;
    @Column(name = "ano_escolaridade_ciclo")
    private String anoEscolaridadeCiclo;
    @Column(name = "ano_escolaridade_fase")
    private String anoEscolaridadeFase;
    @Column(name = "ano_escolaridade_ano")
    private String anoEscolaridadeAno;
    private String naturalidade;
    private String cor;
    @Column(name = "beneficio_bolsa")
    private boolean beneficioBolsa;
    @Column(name = "responsavel_recebimento")
    private String responsavelRecebimento;
    private String ci;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(name = "data_expedicao")
    private Date dataExpedicao;
    private String orgao;
    @Column(name = "nome_mae_recebimento")
    private String nomeMaeRecebimento;
    @Column(name = "inscricaoRecebimento")
    private String inscricaoRecebimento;
    private boolean bpc;
    @Column(name = "alimento_perigoso")
    private boolean alimentoPerigoso;
    @Column(name = "qual_alimento_perigoso")
    private String qualAlimentoPerigoso;
    @Column(name = "problema_saude")
    private boolean problemaSaude;
    @Column(name = "qual_problema_saude")
    private String qualProblemaSaude;
    @Column(name = "medicamente")
    private boolean medicamente;
    @Column(name = "qual_medicamente")
    private String qualMedicamento;
    private String observacao;
    @ManyToOne
    @JoinColumn(name = "id_aluno")
    private Aluno aluno;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

}
