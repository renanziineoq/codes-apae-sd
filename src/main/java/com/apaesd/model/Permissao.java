package com.apaesd.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Data;

/**
 * @author renan
 * @since
 */
@Entity
@Table(name = "permissao")
@Data
public class Permissao extends BasicModel implements Serializable {
    private String permissao;

    public Permissao permissao(final String permissao) {
        this.permissao = permissao;
        return this;
    }

    public Permissao(){}

    @Override
    public String toString() {
        return permissao;
    }

}



