package com.apaesd.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author renan araujo
 * @since 03/09/2019
 */
@Entity
@Table(name = "usuario")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Usuario extends BasicDados {

    @Column(name = "nome_usuario")
    private String nomeUsuario;
    private String senha;
    private String email;
    @OneToOne
    @JoinColumn(name = "id_permissao")
    private Permissao permissao;


}
