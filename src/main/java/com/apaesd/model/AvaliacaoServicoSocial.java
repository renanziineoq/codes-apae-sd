package com.apaesd.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author renan
 * @since 08/09/2019
 */

@Entity
@Table(name = "avaliacao_servico_social")
@Data
public class AvaliacaoServicoSocial extends BasicModel{

    @Column(name = "encaminhado_por")
    private String encaminhadoPor;
    @Column(name = "data_evolucao")
    private Date dataAvaliacao;
    private String identificacao;
    @Column(name = "data_nascimento")
    private Date dataNascimento;
    @Column(name = "documento_id")
    private String documentoId;
    private String cpf;
    private String pai;
    @Column(name = "profissao_pai")
    private String profissaopai;
    private String mae;
    @Column(name = "profissao_mae")
    private String profissaoMae;
    @Column(name = "numero_irmao")
    private Long numeroIrmao;
    @Column(name = "numero_irma")
    private Long numeroIrma;
    @Column(name = "posicao_familiar")
    private String posicaoFamiliar;
    private String endereco;
    @Column(name = "ponto_referencia")
    private String pontoReferencia;
    private String responsavel;
    @Column(name = "beneficio_sus")
    private boolean beneficioSus;
    @Column(name = "beneficio_bolsa")
    private boolean beneficioBolsa;
    private String telefone;
    private String nis;
    private String sus;
    @ManyToOne
    @JoinColumn(name = "id_aluno")
    private Aluno aluno;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    @Column(name = "tipo_area_moradia")
    private String tipoAreaMoradia;
    @Column(name = "tipo_casa")
    private String tipoCasa;
    @Column(name = "valor_aluguel")
    private String valorMoradia;
    @Column(name = "numero_comodos")
    private String numeroComodos;
    private String agua;
    @Column(name = "destino_esgoto")
    private String destinoEsgoto;
    @Column(name = "energia_eletrica")
    private String energiaEletrica;
    private String pavimentacao;
    private String lixo;
    @Column(name = "energia_valor")
    private Float energiaValor;
    @Column(name = "agua_valor")
    private Float aguaValor;
    @Column(name = "gas_valor")
    private Float gasValor;
    @Column(name = "alimentacao_valor")
    private Float alimentacaoValor;
    @Column(name = "transporte_valor")
    private Float transporte;
    @Column(name = "aluguel_valor")
    private Float alguel_valor;
    @Column(name = "medicamente_valor")
    private Float medicamentoValor;
    @Column(name = "meio_transporte")
    private String meioTransporte;
    @Column(name = "tipo_gravidez")
    private String tipoGravidez;
    @Column(name = "teve_aborto")
    private String teveAborto;
    @Column(name = "alimentacao_gravidez")
    private String alimentacaoGravidez;
    @Column(name = "uso_drogas_alcool")
    private String usoDrogaAlcool;
    @Column(name = "qual_droga_alcool")
    private String qualDrogaAlcoo;
    @Column(name = "frequencia_droga_alcool")
    private String frequenciaDrogaAlcoo;
    @Column(name = "realizou_pre_natal")
    private String realizouPreNatal;
    @Column(name = "usou_remedio_gravidez")
    private String usouRemedioGravidez;
    @Column(name = "doenca_gestacao")
    private String doencaGestacao;
    @Column(name = "qual_doenca_gestacao")
    private String qualDoencaGestacao;
    @Column(name = "passou_conturbacao")
    private String passouConturbacao;
    @Column(name = "local_parto")
    private String localParto;
    @Column(name = "outro_local_parto")
    private String outroLocalParto;
    @Column(name = "tipo_parto")
    private String tipoParto;
    @Column(name = "tempo_parto")
    private String tempoParto;
    @Column(name = "chorou_parto")
    private String chorouParto;
    @Column(name = "oxigenio_parto")
    private String oxigenioParto;
    private String incubadora;
    @Column(name = "sugou_nascer")
    private String sugouNascer;
    @Column(name = "chupeta_dedo")
    private String chupetaDedo;
    @Column(name = "engasga_frequentemente")
    private String engasgaFrequentemente;
    @Column(name = "alimenta_bem")
    private String alimentaBem;
    private String sono;
    @Column(name = "quarto_pais")
    private String quartoPais;
    @Column(name = "mesma_cama")
    private String mesmaCama;
    @Column(name = "controle_esfincteres")
    private String controleEsfincteres;
    @Column(name = "doencas_que_teve")
    private String doencasQueTeve;
    @Column(name = "teve_internacao")
    private String teveInternacao;
    @Column(name = "porque_teve_internacao")
    private String porqueTeveInternacao;
    @Column(name = "teve_convulsao")
    private String teveConvulsao;
    @Column(name = "quantas_teve_internacao")
    private String quantasTeveConvulsao;
    @Column(name = "sofreu_desmaio")
    private String sofreuDesmaio;
    @Column(name = "ouve_bem")
    private String ouveBem;
    @Column(name = "exame_auditivo")
    private String exameAuditivo;
    @Column(name = "tratamento_neurologico")
    private String tratamentoNeurologico;
    @Column(name = "medico_tratamento_neurologico")
    private String medicoTratamentoNeurologico;
    private String engatinhar;
    @Column(name = "anos_engatinhar")
    private String anosEngatinhar;
    @Column(name = "corre_salta")
    private String correSalta;
    @Column(name = "cai_facilidade")
    private String caiFacilidade;
    @Column(name = "problema_fala")
    private String problemaFala;
    @Column(name = "coompreende_ordem")
    private String compreendeOrdem;
    @Column(name = "e_responsavel")
    private String eResponsavel;
    @Column(name = "mais_gosta_fazer")
    private String maisGostaFazer;
    @Column(name = "frequenta_local_publico")
    private String frequentaLocalPublico;
    @Column(name = "sozinha_acompanhado")
    private String sozinhoAcompanhado;
    @Column(name = "contato_deficiencia")
    private String contatoDeficiencia;
    @Column(name = "preconceito_descriminacao")
    private String preconceitoDescriminacao;
    @Column(name = "trabalho_casa")
    private String tranalhoCasa;
    @Column(name = "conhece_manipula_dinheiro")
    private String conheceManipulaDinherio;
    @Column(name = "opiniao_solicitada_familia")
    private String opiniaoSolicitadaFamilia;
    private String eleitor;
    private String vota;
    @Column(name = "clube_esportivo")
    private String clubeEsportivo;
    private String creas;
    private String escola;
    private String praca;
    private String psf;
    @Column(name = "crianca_frequenta_escola")
    private String criancaFrequentaEscola;
    @Column(name = "qual_crianca_frequenta_escola")
    private String qualCriancaFrequentaEscola;
    @Column(name = "fez_pre_primario")
    private String fezPrePrimario;
    @Column(name = "adaptacao_escola")
    private String adaptacaoEscola;
    @Column(name = "gosta_escola")
    private String gostaEscola;
    @Column(name = "convivio_colegas")
    private String convivioColegas;
    @Column(name = "dificuldades_apresentadas")
    private String dificuldadesApresentadas;
    @Column(name = "outro_dificuldades_apresentadas")
    private String outroDificuldadesApresentadas;
    @Column(name = "comportamento_escola")
    private String comportamentoEscola;
    @Column(name = "deficiencia_apresenta")
    private String deficienciaApresenta;
    @Column(name = "quando_descobriu_deficiencia")
    private String quandoDescobriuDeficiencia;
    @Column(name = "tipo_exame")
    private String tipoExame;
    @Column(name = "aceitacao_familiares")
    private String aceitacaoFamiliares;
    @Column(name = "faz_tratamento_com")
    private String fazTratamentoCom;
    @Column(name = "faz_uso_medicacao")
    private String fazUsoMedicacao;
    private String observacoes;


}
