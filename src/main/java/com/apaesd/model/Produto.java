package com.apaesd.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author renan
 * @since 08/09/2019
 */
@Entity
@Table(name = "produto")
@Data
public class Produto extends BasicModel {

    @Column(name = "nome_produto")
    private String nomeProduto;

    @Column(name = "tipo_produto")
    private String tipoProduto;

    private Float quantidade;

    @Column(name = "tipo_quantidade")
    private String tipoQuantidade;

    private String descricao;

}
