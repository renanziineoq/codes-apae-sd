package com.apaesd.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author renan
 * @since 09/09/2019
 */
@MappedSuperclass
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BasicDados extends BasicModel {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected String nome;
    protected String idade;
    @Column(name = "data_nascimento")
    protected Date dataNascimento;
    protected String uf;
    protected String cidade;
    protected String rua;
    protected String numero;
    protected String bairro;
    protected String cep;
    protected String telefone;
    protected String celular;
    protected String sexo;
}
