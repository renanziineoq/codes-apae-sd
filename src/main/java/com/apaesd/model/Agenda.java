package com.apaesd.model;

import javax.persistence.*;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author renan
 * @since 08/09/2019
 */

@Entity
@Table(name = "agenda")
@Data
public class Agenda extends BasicModel {

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(name = "data_agenda")
    private Date dataAgenda;
    @ManyToOne
    @JoinColumn(name = "id_aluno")
    private Aluno aluno;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    private String lembrete;

}
