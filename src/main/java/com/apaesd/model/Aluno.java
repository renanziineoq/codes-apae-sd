package com.apaesd.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author renan
 * @since 08/09/2019
 */

@Entity
@Table(name = "aluno")
@Data
public class Aluno extends BasicDados {

    @Column(name = "pai")
    private String pai;
    @Column(name = "mae")
    private String mae;
    @Column(name = "responsavel")
    private String responsavel;
    @Column(name = "descricao")
    private String descricao;
    private Boolean status;



}