package com.apaesd.dto;

import com.apaesd.model.Aluno;
import com.apaesd.model.BasicModel;
import com.apaesd.model.Usuario;

import java.util.Date;

public class TriagemFonoaudiologicaDTO {

    private Long id;
    private String nome;
    private Usuario usuario;
    private Date dataTriagem;
    private String escola;
    private String encaminhado;
    private String queixa;
    private String ofas;
    private String respiracao;
    private String voz;
    private String audicao;
    private String comunicacao;
    private String disfluencia;
    private String realfa;
    private String omissoes;
    private String distorcoes;
    private String substituicoes;
    private String encaminhamentos;
    private String atendimentoAudiologia;
    private String atendimentoLinguagem;
    private String atendimentoMotrocidadeOrofacial;
    private String atendimentoVoz;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getDataTriagem() {
        return dataTriagem;
    }

    public void setDataTriagem(Date dataTriagem) {
        this.dataTriagem = dataTriagem;
    }

    public String getEscola() {
        return escola;
    }

    public void setEscola(String escola) {
        this.escola = escola;
    }

    public String getEncaminhado() {
        return encaminhado;
    }

    public void setEncaminhado(String encaminhado) {
        this.encaminhado = encaminhado;
    }

    public String getQueixa() {
        return queixa;
    }

    public void setQueixa(String queixa) {
        this.queixa = queixa;
    }

    public String getOfas() {
        return ofas;
    }

    public void setOfas(String ofas) {
        this.ofas = ofas;
    }

    public String getRespiracao() {
        return respiracao;
    }

    public void setRespiracao(String respiracao) {
        this.respiracao = respiracao;
    }

    public String getVoz() {
        return voz;
    }

    public void setVoz(String voz) {
        this.voz = voz;
    }

    public String getAudicao() {
        return audicao;
    }

    public void setAudicao(String audicao) {
        this.audicao = audicao;
    }

    public String getComunicacao() {
        return comunicacao;
    }

    public void setComunicacao(String comunicacao) {
        this.comunicacao = comunicacao;
    }

    public String getDisfluencia() {
        return disfluencia;
    }

    public void setDisfluencia(String disfluencia) {
        this.disfluencia = disfluencia;
    }

    public String getRealfa() {
        return realfa;
    }

    public void setRealfa(String realfa) {
        this.realfa = realfa;
    }

    public String getOmissoes() {
        return omissoes;
    }

    public void setOmissoes(String omissoes) {
        this.omissoes = omissoes;
    }

    public String getDistorcoes() {
        return distorcoes;
    }

    public void setDistorcoes(String distorcoes) {
        this.distorcoes = distorcoes;
    }

    public String getSubstituicoes() {
        return substituicoes;
    }

    public void setSubstituicoes(String substituicoes) {
        this.substituicoes = substituicoes;
    }

    public String getEncaminhamentos() {
        return encaminhamentos;
    }

    public void setEncaminhamentos(String encaminhamentos) {
        this.encaminhamentos = encaminhamentos;
    }

    public String getAtendimentoAudiologia() {
        return atendimentoAudiologia;
    }

    public void setAtendimentoAudiologia(String atendimentoAudiologia) {
        this.atendimentoAudiologia = atendimentoAudiologia;
    }

    public String getAtendimentoLinguagem() {
        return atendimentoLinguagem;
    }

    public void setAtendimentoLinguagem(String atendimentoLinguagem) {
        this.atendimentoLinguagem = atendimentoLinguagem;
    }

    public String getAtendimentoMotrocidadeOrofacial() {
        return atendimentoMotrocidadeOrofacial;
    }

    public void setAtendimentoMotrocidadeOrofacial(String atendimentoMotrocidadeOrofacial) {
        this.atendimentoMotrocidadeOrofacial = atendimentoMotrocidadeOrofacial;
    }

    public String getAtendimentoVoz() {
        return atendimentoVoz;
    }

    public void setAtendimentoVoz(String atendimentoVoz) {
        this.atendimentoVoz = atendimentoVoz;
    }
}
