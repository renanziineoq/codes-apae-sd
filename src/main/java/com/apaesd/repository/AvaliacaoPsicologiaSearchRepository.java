package com.apaesd.repository;

import com.apaesd.model.AvaliacaoPsicologia;
import com.apaesd.model.LinhaSemanal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AvaliacaoPsicologiaSearchRepository extends JpaRepository<AvaliacaoPsicologia, Long> {

    @Query("SELECT L FROM AvaliacaoPsicologia L WHERE L.aluno.nome LIKE %?1%")
    List<AvaliacaoPsicologia> findByNomeAlunoAvaliacaoPsciologia(final String nome);

    @Query("SELECT L FROM AvaliacaoPsicologia L WHERE L.usuario.nome LIKE %?1%")
    List<AvaliacaoPsicologia> findByNomeUsuarioAvaliacaoPsciologia(final String nome);

}
