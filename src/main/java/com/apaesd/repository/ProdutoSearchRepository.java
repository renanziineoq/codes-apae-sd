package com.apaesd.repository;

import com.apaesd.model.Aluno;
import com.apaesd.model.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProdutoSearchRepository extends JpaRepository<Produto, Long> {

    @Query("SELECT P FROM Produto P WHERE P.nomeProduto LIKE %?1%")
    List<Produto> findByNomeProduto(final String nomep);

}
