package com.apaesd.repository;

import com.apaesd.model.TriagemFonoaudiologica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TriagemFonoaudiologiaRepository extends JpaRepository<TriagemFonoaudiologica, Long> {
    TriagemFonoaudiologica findByEncaminhado(String encaminhado);


}
