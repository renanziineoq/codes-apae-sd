package com.apaesd.repository;

import com.apaesd.model.Aluno;
import com.apaesd.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UsuarioSearchRepository extends JpaRepository<Aluno, Long> {

    @Query("SELECT U FROM Usuario U WHERE U.nome LIKE %?1%")
    List<Usuario> findByNomeUsuario(final String nome);

}
