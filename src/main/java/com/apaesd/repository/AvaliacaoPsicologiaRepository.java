package com.apaesd.repository;

import com.apaesd.model.AvaliacaoPsicologia;
import com.apaesd.model.MatriculaAluno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AvaliacaoPsicologiaRepository extends JpaRepository<AvaliacaoPsicologia, Long> {
   AvaliacaoPsicologia findByConcepcao(String concepcao);
}
