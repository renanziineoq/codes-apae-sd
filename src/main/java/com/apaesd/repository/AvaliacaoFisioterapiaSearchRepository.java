package com.apaesd.repository;

import com.apaesd.model.AvaliacaoFisioterapia;
import com.apaesd.model.AvaliacaoPsicologia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AvaliacaoFisioterapiaSearchRepository extends JpaRepository<AvaliacaoFisioterapia, Long> {

    @Query("SELECT L FROM AvaliacaoFisioterapia L WHERE L.aluno.nome LIKE %?1%")
    List<AvaliacaoFisioterapia> findByNomeAlunoAvaliacaoFisioterapia(final String nome);

    @Query("SELECT L FROM AvaliacaoFisioterapia L WHERE L.usuario.nome LIKE %?1%")
    List<AvaliacaoFisioterapia> findByNomeUsuarioAvaliacaoFisioterapia(final String nome);

}
