package com.apaesd.repository;

import com.apaesd.model.Aluno;
import com.apaesd.model.LinhaSemanal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LinhaSemanalSearchRepository extends JpaRepository<LinhaSemanal, Long> {

    @Query("SELECT L FROM LinhaSemanal L WHERE L.aluno.nome LIKE %?1%")
    List<LinhaSemanal> findByNomeAlunoLinhaSemanal(final String nome);

}
