package com.apaesd.repository;

import com.apaesd.model.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AlunoSearchRepository extends JpaRepository<Aluno, Long> {

    @Query("SELECT A FROM Aluno A WHERE A.nome LIKE %?1%")
    List<Aluno> findByNomeAluno(final String nome);

}
