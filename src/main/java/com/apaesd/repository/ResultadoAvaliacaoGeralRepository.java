package com.apaesd.repository;


import com.apaesd.model.ResultadoAvaliacaoGeral;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Rodrigo Moliterno
 * @since 16/11/2019
 */
public interface ResultadoAvaliacaoGeralRepository  extends JpaRepository<ResultadoAvaliacaoGeral, Long> {
ResultadoAvaliacaoGeral findByMotivo(String motivo);

}
