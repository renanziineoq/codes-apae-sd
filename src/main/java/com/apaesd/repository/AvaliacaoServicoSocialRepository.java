package com.apaesd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apaesd.model.AvaliacaoServicoSocial;

/**
 * @author Rodrigo Moliterno
 * @since 22/11/2019
 */

public interface AvaliacaoServicoSocialRepository extends JpaRepository<AvaliacaoServicoSocial, Long> {

}
