package com.apaesd.repository;

import com.apaesd.model.Agenda;
import com.apaesd.model.LinhaSemanal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LinhaSemanalRepository extends JpaRepository<LinhaSemanal, Long> {
    Agenda findByDescricao(String lembrete);
}
