package com.apaesd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apaesd.model.Usuario;

public interface UserRepository extends JpaRepository<Usuario, Long>{
    Usuario findByNomeUsuario(String nomeUsuario);
}
