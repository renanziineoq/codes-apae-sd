package com.apaesd.repository;

import com.apaesd.model.MatriculaAluno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatriculaAlunoRepository extends JpaRepository<MatriculaAluno, Long> {
    MatriculaAluno findByObservacao(String observacao);
}
