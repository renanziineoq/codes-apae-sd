package com.apaesd.repository;

import com.apaesd.model.Agenda;
import com.apaesd.model.AvaliacaoFisioterapia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AgendaSearchRepository extends JpaRepository<Agenda, Long> {

    @Query("SELECT L FROM Agenda L WHERE L.aluno.nome LIKE %?1%")
    List<Agenda> findByNomeAlunoAvaliacaoFisioterapia(final String nome);

    @Query("SELECT L FROM Agenda L WHERE L.usuario.nome LIKE %?1%")
    List<Agenda> findByNomeUsuarioAvaliacaoFisioterapia(final String nome);

}
