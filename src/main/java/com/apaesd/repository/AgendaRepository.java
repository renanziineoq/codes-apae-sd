package com.apaesd.repository;

import com.apaesd.model.Agenda;
import com.apaesd.model.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgendaRepository extends JpaRepository<Agenda, Long> {
    Agenda findByLembrete(String lembrete);
}
