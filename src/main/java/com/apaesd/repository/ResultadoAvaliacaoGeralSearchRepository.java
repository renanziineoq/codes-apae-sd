package com.apaesd.repository;

import com.apaesd.model.ResultadoAvaliacaoGeral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ResultadoAvaliacaoGeralSearchRepository extends JpaRepository<ResultadoAvaliacaoGeral, Long> {

    @Query("SELECT L FROM ResultadoAvaliacaoGeral L WHERE L.aluno.nome LIKE %?1%")
    List<ResultadoAvaliacaoGeral> findByNomeAlunoAvaliacaoGeral(final String nome);


}
