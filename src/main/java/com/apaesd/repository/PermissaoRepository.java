package com.apaesd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apaesd.model.Permissao;

public interface PermissaoRepository extends JpaRepository<Permissao, Long> {
}
