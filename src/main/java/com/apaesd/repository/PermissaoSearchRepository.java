package com.apaesd.repository;

import com.apaesd.model.Permissao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PermissaoSearchRepository extends JpaRepository<Permissao, Long> {

    @Query("SELECT P FROM Permissao P WHERE P.permissao LIKE %?1%")
    List<Permissao> findByPermissao(final String permissao);

}