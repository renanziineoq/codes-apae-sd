package com.apaesd.repository;

import com.apaesd.model.MatriculaAluno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MatriculaAlunoSearchRepository extends JpaRepository<MatriculaAluno, Long> {

    @Query("SELECT L FROM MatriculaAluno L WHERE L.aluno.nome LIKE %?1%")
    List<MatriculaAluno> findByMatriculaNomeAluno(final String nome);

}