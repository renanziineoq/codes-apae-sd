package com.apaesd.repository;

import com.apaesd.model.ResultadoAvaliacaoGeral;
import com.apaesd.model.TriagemFonoaudiologica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TriagemFonoAudiologiaSearchRepository extends JpaRepository<TriagemFonoaudiologica, Long> {

    @Query("SELECT L FROM TriagemFonoaudiologica L WHERE L.aluno.nome LIKE %?1%")
    List<TriagemFonoaudiologica> findByNomeAlunoTriagem(final String nome);

    @Query("SELECT L FROM TriagemFonoaudiologica L WHERE L.usuario.nome LIKE %?1%")
    List<TriagemFonoaudiologica> findByNomeUsuarioTriagem(final String nome);


}
