package com.apaesd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apaesd.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

    Produto findByDescricao(String descricao);
}
