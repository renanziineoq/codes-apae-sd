package com.apaesd.repository;

import com.apaesd.model.AvaliacaoFisioterapia;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AvaliacaoFisiotarapiaRepository extends JpaRepository<AvaliacaoFisioterapia, Long> {
    AvaliacaoFisioterapia findByConclusao(String conclusao);
}
