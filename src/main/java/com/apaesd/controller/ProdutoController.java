package com.apaesd.controller;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.apaesd.model.Produto;
import com.apaesd.repository.ProdutoRepository;
import com.apaesd.repository.ProdutoSearchRepository;
import com.apaesd.service.ReportService;
import lombok.extern.java.Log;
import net.sf.jasperreports.engine.JRException;

/**
 * @author Rodrigo Moliterno
 * @since 16/11/2019
 */
@Controller
@Log
public class ProdutoController {

    @Autowired
    ProdutoSearchRepository produtoSearchRepository;

    @Autowired
    ProdutoRepository produtoRepository;

    @Autowired
    ReportService service;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/produto", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        final ModelAndView mv = new ModelAndView("produto/index");
        mv.addObject("produtos", produtoRepository.findAll());
        return mv;
    }

    @GetMapping("/produto/view/{idProduto}")
    public ModelAndView viewPage(@PathVariable("idProduto") final Produto produto) {
        final ModelAndView mv = new ModelAndView("produto/VisualizarProduto");
        mv.addObject("produto", produto);
        return mv;
    }

    @RequestMapping(value = "/produto/lista", method = RequestMethod.GET)
    public ModelAndView produtos(@ModelAttribute("current") final Produto current) {
        final ModelAndView mv = new ModelAndView("produto/listaProduto");
        if (current != null && current.getNomeProduto() != null) {
            mv.addObject("produtos", produtoSearchRepository.findByNomeProduto(current.getNomeProduto()));
        } else {
            mv.addObject("produtos", produtoRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }



    @RequestMapping(value = "/produto/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") final Produto current) {
        final ModelAndView mv = new ModelAndView("produto/cadastrarProduto");
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/produto/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@ModelAttribute("current") final @Valid Produto current, final BindingResult result) {
        System.out.println(current);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        produtoRepository.save(current);
        return new ModelAndView("redirect:/produto/lista");
    }

    @RequestMapping(value = "/produto/edit", method = RequestMethod.GET)
    public ModelAndView editars(@ModelAttribute("editProduto") final Produto edit) {
        final ModelAndView mv = new ModelAndView("produto/alterarProduto");
        mv.addObject("editProduto", produtoRepository.findById(edit.getId()).orElse(null));
        mv.addObject(edit);
        return mv;
    }

    @RequestMapping(value = "/produto/edit", method = RequestMethod.POST)
    @Transactional
    public ModelAndView saveedit(@ModelAttribute("editProduto") final @Valid Produto edit, final BindingResult result) {
        System.out.println(edit);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return editars(edit);
        }
        produtoRepository.save(edit);
        return new ModelAndView("redirect:/produto/lista");
    }

    @GetMapping("/produto/edit/{id}")
    public ModelAndView editar(@PathVariable("id") @ModelAttribute("id") final Produto current) {
        final ModelAndView mv = new ModelAndView("produto/alterarProduto");
        mv.addObject("editProduto", produtoRepository.findById(current.getId()).orElse(null));
        return editars(current);
    }

    @RequestMapping(path = "/produto/delete/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable(name = "id") final Long id) {
        produtoRepository.deleteById(id);
        return "redirect:/produto/lista";
    }

    @RequestMapping(path = "/report/produto", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public @ResponseBody byte[] generateReport() throws IOException, JRException {
        final String path = service.exportReportProduto();
        final File arquivo = new File(path);
        InputStream in = new FileInputStream(arquivo);
        return IOUtils.toByteArray(in);
    }
}
