package com.apaesd.controller;

import com.apaesd.model.AvaliacaoPsicologia;
import com.apaesd.model.TriagemFonoaudiologica;
import com.apaesd.repository.AlunoRepository;
import com.apaesd.repository.TriagemFonoAudiologiaSearchRepository;
import com.apaesd.repository.TriagemFonoaudiologiaRepository;
import com.apaesd.repository.UserRepository;
import com.apaesd.service.ReportService;
import lombok.extern.java.Log;
import net.sf.jasperreports.engine.JRException;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Log
public class TriagemFonoaudiologiaController {

    public static final String ACTION_SAVE = "Salvar";
    @Autowired
    TriagemFonoaudiologiaRepository triagemFonoaudiologiaRepository;
    @Autowired
    AlunoRepository alunoRepository;
    @Autowired
    TriagemFonoAudiologiaSearchRepository triagemFonoAudiologiaSearchRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ReportService service;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/triagemfonoaudiologia", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        ModelAndView mv = new ModelAndView("triagemfonoaudiologia/index");
        mv.addObject("triagemfonoaudiologias", triagemFonoaudiologiaRepository.findAll());
        return mv;
    }

    @GetMapping(value = "/triagemfonoaudiologia/view/{idTriagemFonoaudiologia}")
    public ModelAndView viewPage(@PathVariable("idTriagemFonoaudiologia") TriagemFonoaudiologica triagemFonoaudiologica) {
        ModelAndView mv = new ModelAndView("triagemfonoaudiologia/VisualizarTriagemFonoaudiologia");
        mv.addObject("triagemfonoaudiologia", triagemFonoaudiologica);
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        return mv;
    }


    @RequestMapping(value = "/triagemfonoaudiologia/lista", method = RequestMethod.GET)
    public ModelAndView triagemfonoaudiologias(@ModelAttribute("current") AvaliacaoPsicologia current) {
        ModelAndView mv = new ModelAndView("triagemfonoaudiologia/listaTriagemFonoaudiologia");
        if (current != null && current.getAluno() != null && current.getAluno().getNome() != null) {
            mv.addObject("triagemfonoaudiologias",
                    triagemFonoAudiologiaSearchRepository.findByNomeAlunoTriagem(current.getAluno().getNome()));
            triagemFonoAudiologiaSearchRepository.findByNomeUsuarioTriagem(current.getUsuario().getNome());
        } else {
            mv.addObject("triagemfonoaudiologias", triagemFonoaudiologiaRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/triagemfonoaudiologia/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") TriagemFonoaudiologica current) {
        ModelAndView mv = new ModelAndView("triagemfonoaudiologia/cadastrarTriagemFonoaudiologia");
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/triagemfonoaudiologia/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid @ModelAttribute("current") TriagemFonoaudiologica current, BindingResult result) {
        System.out.println(current);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        triagemFonoaudiologiaRepository.save(current);
        return new ModelAndView("redirect:/triagemfonoaudiologia/lista");
    }

    @RequestMapping(value = "/triagemfonoaudiologia/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TriagemFonoaudiologica>> getAllGrupos() {
        List<TriagemFonoaudiologica> triagemFonoaudiologicas = triagemFonoaudiologiaRepository.findAll();
        return new ResponseEntity<>(triagemFonoaudiologicas, HttpStatus.OK);
    }

    @RequestMapping(path = "/report/triagemfonoaudiologia", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public @ResponseBody byte[] generateReport() throws IOException, JRException {
        final String path = service.exportReportTriagemFonoaudiologica();
        final File arquivo = new File(path);
        InputStream in = new FileInputStream(arquivo);
        return IOUtils.toByteArray(in);
    }
}
