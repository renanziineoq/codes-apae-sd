package com.apaesd.controller;


import com.apaesd.model.Permissao;
import com.apaesd.repository.PermissaoRepository;
import com.apaesd.repository.PermissaoSearchRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Log
public class PermissaoController {


    @Autowired
    PermissaoRepository permissaoRepository;

    @Autowired
    PermissaoSearchRepository permissaoSearchRepository;

    @RequestMapping(value = "/permissao", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        final ModelAndView mv = new ModelAndView("permissao/index");
        mv.addObject("permissoes", permissaoRepository.findAll());
        return mv;
    }

    @GetMapping("/permissao/view/{idPermissao}")
    public ModelAndView viewPage(@PathVariable("idPermissao") final Permissao permissao) {
        final ModelAndView mv = new ModelAndView("permissao/visualizar");
        mv.addObject("permissao", permissao);
        return mv;
    }

    @RequestMapping(value = "/permissao/lista", method = RequestMethod.GET)
    public ModelAndView permissoes(@ModelAttribute("current") final Permissao current) {
        final ModelAndView mv = new ModelAndView("permissao/listaPermissao");
        if (current != null && current.getPermissao() != null) {
            mv.addObject("permissoes", permissaoSearchRepository.findByPermissao(current.getPermissao()));
        } else {
            mv.addObject("permissoes", permissaoRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/permissao/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") final Permissao current) {
        final ModelAndView mv = new ModelAndView("permissao/cadastrarPermissao");
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/permissao/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@ModelAttribute("current") final @Valid Permissao current, final BindingResult result) {
        System.out.println(current);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        permissaoRepository.save(current);
        return new ModelAndView("redirect:/permissao/lista");
    }

    @RequestMapping(value = "/permissao/edit", method = RequestMethod.GET)
    public ModelAndView editars(@ModelAttribute("editpermissao") final Permissao edit) {
        final ModelAndView mv = new ModelAndView("permissao/alterarPermissao");
        mv.addObject("editpermissao", permissaoRepository.findById(edit.getId()).orElse(null));
        mv.addObject(edit);
        return mv;
    }

    @RequestMapping(value = "/permissao/edit", method = RequestMethod.POST)
    @Transactional
    public ModelAndView saveedit(@ModelAttribute("editpermissao") final @Valid Permissao edit, final BindingResult result) {
        System.out.println(edit);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return editars(edit);
        }
        permissaoRepository.save(edit);
        return new ModelAndView("redirect:/permissao/lista");
    }

    @GetMapping("/permissao/edit/{id}")
    public ModelAndView editar(@PathVariable("id") @ModelAttribute("id") final Permissao current) {
        final ModelAndView mv = new ModelAndView("permissao/alterarPermissao");
        mv.addObject("editpermissao", permissaoRepository.findById(current.getId()).orElse(null));
        return editars(current);
    }

    @RequestMapping(path = "/permissao/delete/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable(name = "id") final Long id) {
        permissaoRepository.deleteById(id);
        return "redirect:/permissao/lista";
    }

}
