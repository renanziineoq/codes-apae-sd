package com.apaesd.controller;

import com.apaesd.model.LinhaSemanal;
import com.apaesd.repository.AlunoRepository;
import com.apaesd.repository.LinhaSemanalRepository;
import com.apaesd.repository.LinhaSemanalSearchRepository;
import com.apaesd.repository.UserRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Log
public class LinhaSemanalController {

    @Autowired
    LinhaSemanalRepository linhaSemanalRepository;

    @Autowired
    LinhaSemanalSearchRepository linhaSemanalSearchRepository;

    @Autowired
    private AlunoRepository alunoRepository;

    @Autowired
    private UserRepository userRepository;

    public static final String ACTION_SAVE = "Salvar";

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/linhasemanal", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        ModelAndView mv = new ModelAndView("linhasemanal/index");
        mv.addObject("linhasemanais", linhaSemanalRepository.findAll());
        return mv;
    }

    @GetMapping(value = "/linhasemanal/view/{idLinhaSemanal}")
    public ModelAndView viewPage(@PathVariable("idLinhaSemanal")LinhaSemanal linhaSemanal) {
        ModelAndView mv = new ModelAndView("linhasemanal/VisualizarLinhaSemanal");
        mv.addObject("linhasemanal",linhaSemanal);
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        return mv;
    }

    @RequestMapping(value = "/linhasemanal/lista", method = RequestMethod.GET)
    public ModelAndView linhasemanais(@ModelAttribute("current") LinhaSemanal current) {
        ModelAndView mv = new ModelAndView("linhasemanal/listaLinhaSemanal");
        if (current != null && current.getAluno() != null && current.getAluno().getNome() != null) {
            mv.addObject("linhasemanais", linhaSemanalSearchRepository.findByNomeAlunoLinhaSemanal(current.getAluno().getNome()));
        }
        else {
            mv.addObject("linhasemanais", linhaSemanalRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/linhasemanal/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") LinhaSemanal current) {
        ModelAndView mv = new ModelAndView("linhasemanal/cadastrarLinhaSemanal");
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/linhasemanal/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid @ModelAttribute("current") LinhaSemanal current, BindingResult result) {
        System.out.println(current);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        linhaSemanalRepository.save(current);
        return new ModelAndView("redirect:/linhasemanal/lista");
    }

    @RequestMapping(value = "/linhasemanal/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<LinhaSemanal>> getAllGrupos() {
        List<LinhaSemanal> linhaSemanais = linhaSemanalRepository.findAll();
        return new ResponseEntity<>(linhaSemanais, HttpStatus.OK);
    }


}
