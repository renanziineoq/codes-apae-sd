package com.apaesd.controller;

import com.apaesd.model.AvaliacaoPsicologia;
import com.apaesd.repository.AlunoRepository;
import com.apaesd.repository.AvaliacaoPsicologiaRepository;
import com.apaesd.repository.AvaliacaoPsicologiaSearchRepository;
import com.apaesd.repository.UserRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Log
public class AvaliacaoPsicologiaController {

    @Autowired
    AvaliacaoPsicologiaRepository avaliacaoPsicologiaRepository;

    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AvaliacaoPsicologiaSearchRepository avaliacaoPsicologiaSearchRepository;

    public static final String ACTION_SAVE = "Salvar";

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/avaliacaopsicologia", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        ModelAndView mv = new ModelAndView("avaliacaopsicologia/index");
        mv.addObject("avaliacaopsicologias", avaliacaoPsicologiaRepository.findAll());
        return mv;
    }

    @GetMapping(value = "/avaliacaopsicologia/view/{idAvaliacaopsicologia}")
    public ModelAndView viewPage(@PathVariable("idAvaliacaopsicologia") AvaliacaoPsicologia avaliacaoPsicologia) {
        ModelAndView mv = new ModelAndView("avaliacaopsicologia/VisualizarAvaliacaoPsicologia");
        mv.addObject("avaliacaopsicologia",avaliacaoPsicologia);
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        return mv;
    }


            @RequestMapping(value = "/avaliacaopsicologia/lista", method = RequestMethod.GET)
            public ModelAndView avaliacaopsicologicas(@ModelAttribute("current") AvaliacaoPsicologia current) {
                ModelAndView mv = new ModelAndView("avaliacaopsicologia/listaAvaliacaoPsicologia");
                if (current != null && current.getAluno() != null && current.getAluno().getNome() != null) {
                    mv.addObject("avaliacaopsicologias", avaliacaoPsicologiaSearchRepository.findByNomeAlunoAvaliacaoPsciologia(current.getAluno().getNome()));
                    mv.addObject("avaliacaopsicologias", avaliacaoPsicologiaSearchRepository.findByNomeUsuarioAvaliacaoPsciologia(current.getUsuario().getNome()));
                }
                else if(current != null && current.getUsuario() != null && current.getUsuario().getNome() != null) {

                    mv.addObject("avaliacaopsicologias", avaliacaoPsicologiaSearchRepository.findByNomeUsuarioAvaliacaoPsciologia(current.getUsuario().getNome()));
                }
                else {
                    mv.addObject("avaliacaopsicologias", avaliacaoPsicologiaRepository.findAll());
                }
                mv.addObject(current);
                return mv;
            }



    @RequestMapping(value = "/avaliacaopsicologia/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") AvaliacaoPsicologia current) {
        ModelAndView mv = new ModelAndView("avaliacaopsicologia/cadastrarAvaliacaoPsicologia");
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/avaliacaopsicologia/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid @ModelAttribute("current") AvaliacaoPsicologia current, BindingResult result) {
        System.out.println(current);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        avaliacaoPsicologiaRepository.save(current);
        return new ModelAndView("redirect:/avaliacaopsicologia/lista");
    }

    @RequestMapping(value = "/avaliacaopsicologia/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AvaliacaoPsicologia>> getAllGrupos() {
        List<AvaliacaoPsicologia> avaliacaoPsicologias = avaliacaoPsicologiaRepository.findAll();
        return new ResponseEntity<>(avaliacaoPsicologias, HttpStatus.OK);
    }


}
