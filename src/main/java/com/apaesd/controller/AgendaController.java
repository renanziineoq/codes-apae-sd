package com.apaesd.controller;

import com.apaesd.model.Agenda;
import com.apaesd.repository.AgendaRepository;
import com.apaesd.repository.AgendaSearchRepository;
import com.apaesd.repository.AlunoRepository;
import com.apaesd.repository.UserRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Log
public class AgendaController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    AgendaRepository agendaRepository;

    @Autowired
    AgendaSearchRepository agendaSearchRepository;

    @Autowired
    private AlunoRepository alunoRepository;

    @Autowired
    private UserRepository userRepository;

    public static final String ACTION_SAVE = "Salvar";

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/agenda", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        ModelAndView mv = new ModelAndView("agenda/index");
        mv.addObject("agendas", agendaRepository.findAll());
        return mv;
    }

    @GetMapping(value = "/agenda/view/{idAgenda}")
    public ModelAndView viewPage(@PathVariable("idAgenda") Agenda agenda) {
        ModelAndView mv = new ModelAndView("agenda/VisualizarAgenda");
        mv.addObject("agenda", agenda);
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        return mv;
    }

    @RequestMapping(value = "/agenda/lista", method = RequestMethod.GET)
    public ModelAndView avaliacaopsicologicas(@ModelAttribute("current") Agenda current) {
        ModelAndView mv = new ModelAndView("agenda/listaAgenda");
        if (current != null && current.getAluno() != null && current.getAluno().getNome() != null) {
            mv.addObject("agendas", agendaSearchRepository.findByNomeAlunoAvaliacaoFisioterapia(current.getAluno().getNome()));
            mv.addObject("agendas", agendaSearchRepository.findByNomeUsuarioAvaliacaoFisioterapia(current.getUsuario().getNome()));
        }
        else {
            mv.addObject("agendas", agendaSearchRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/agenda/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("currente") Agenda current) {
        ModelAndView mv = new ModelAndView("agenda/cadastrarAgenda");
        mv.addObject("tiposUsuarios", userRepository.findAll());
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/agenda/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid @ModelAttribute("currente") Agenda current, BindingResult result) {
        System.out.println(current);
        if(result.hasErrors()){
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        agendaRepository.save(current);
        return new ModelAndView("redirect:/agenda/lista");
    }

    @RequestMapping(value = "/agendas/edit", method = RequestMethod.GET)
    public ModelAndView editars(@ModelAttribute("current") Agenda edit) {
        ModelAndView mv = new ModelAndView("agenda/alterarAgenda");
        mv.addObject("editAgenda", agendaRepository.findById(edit.getId()).get());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject(edit);
        return mv;
    }

    @RequestMapping(value = "/agendas/edit", method = RequestMethod.POST)
    @Transactional
    public ModelAndView saveedit(@Valid @ModelAttribute("editAgenda") Agenda edit, BindingResult result) {
        System.out.println(edit);
        if(result.hasErrors()){
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return editars(edit);
        }
        agendaRepository.save(edit);
        return new ModelAndView("redirect:/agenda/lista");
    }

    @GetMapping(value = "/agendas/edit/{id}")
    public ModelAndView editar(@PathVariable("id") @ModelAttribute("id") Agenda current) {
        ModelAndView mv = new ModelAndView("agenda/alterarAgenda");
        mv.addObject("editAgenda", agendaRepository.findById(current.getId()));
        return editars(current);
    }

    @RequestMapping(value = "/agenda/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Agenda>> getAllGrupos() {
        List<Agenda> agendas = agendaRepository.findAll();
        return new ResponseEntity<>(agendas, HttpStatus.OK);
    }

    @RequestMapping(path = "/agenda/delete/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable(name = "id") Long id) {
        agendaRepository.deleteById(id);
        return "redirect:/agenda/lista";
    }

}
