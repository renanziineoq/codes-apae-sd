package com.apaesd.controller;


import com.apaesd.model.AvaliacaoFisioterapia;
import com.apaesd.repository.AlunoRepository;
import com.apaesd.repository.AvaliacaoFisiotarapiaRepository;
import com.apaesd.repository.AvaliacaoFisioterapiaSearchRepository;
import com.apaesd.repository.UserRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Log
public class AvaliacaoFisioterapiaController {

    @Autowired
    AvaliacaoFisiotarapiaRepository avaliacaoFisiotarapiaRepository;

    @Autowired
    AvaliacaoFisioterapiaSearchRepository avaliacaoFisioterapiaSearchRepository;

    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    UserRepository userRepository;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/avaliacaofisioterapia", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        ModelAndView mv = new ModelAndView("avaliacaofisioterapia/index");
        mv.addObject("avaliacaofisioterapias", avaliacaoFisiotarapiaRepository.findAll());
        return mv;
    }

    @GetMapping(value = "/avaliacaofisioterapia/view/{idAvaliacaofisioterapia}")
    public ModelAndView viewPage(@PathVariable("idAvaliacaofisioterapia") AvaliacaoFisioterapia avaliacaoFisioterapia) {
        ModelAndView mv = new ModelAndView("avaliacaofisioterapia/VisualizarAvaliacaoFisioterapia");
        mv.addObject("avaliacaofisioterapia",avaliacaoFisioterapia);
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        return mv;
    }


    @RequestMapping(value = "/avaliacaofisioterapia/lista", method = RequestMethod.GET)
    public ModelAndView avaliacaopsicologicas(@ModelAttribute("current") AvaliacaoFisioterapia current) {
        ModelAndView mv = new ModelAndView("avaliacaofisioterapia/listaAvaliacaoFisioterapia");
        if (current != null && current.getAluno() != null && current.getAluno().getNome() != null) {
            mv.addObject("avaliacaofisioterapias", avaliacaoFisioterapiaSearchRepository.findByNomeAlunoAvaliacaoFisioterapia(current.getAluno().getNome()));
            mv.addObject("avaliacaofisioterapias", avaliacaoFisioterapiaSearchRepository.findByNomeUsuarioAvaliacaoFisioterapia(current.getUsuario().getNome()));
        }
        else {
            mv.addObject("avaliacaofisioterapias", avaliacaoFisiotarapiaRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }



    @RequestMapping(value = "/avaliacaofisioterapia/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") AvaliacaoFisioterapia current) {
        ModelAndView mv = new ModelAndView("avaliacaofisioterapia/cadastrarAvaliacaoFisioterapia");
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/avaliacaofisioterapia/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid @ModelAttribute("current") AvaliacaoFisioterapia current, BindingResult result) {
        System.out.println(current);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        avaliacaoFisiotarapiaRepository.save(current);
        return new ModelAndView("redirect:/avaliacaofisioterapia/lista");
    }

    @RequestMapping(value = "/avaliacaofisioterapia/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AvaliacaoFisioterapia>> getAllGrupos() {
        List<AvaliacaoFisioterapia> avaliacaoFisioterapias = avaliacaoFisiotarapiaRepository.findAll();
        return new ResponseEntity<>(avaliacaoFisioterapias, HttpStatus.OK);
    }

}
