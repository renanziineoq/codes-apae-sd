package com.apaesd.controller;

import com.apaesd.model.MatriculaAluno;
import com.apaesd.repository.*;
import lombok.extern.java.Log;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Log
public class MatriculaAlunoController {

    @Autowired
    MatriculaAlunoRepository matriculaAlunoRepository;

    @Autowired
    MatriculaAlunoSearchRepository matriculaAlunoSearchRepository;

    @Autowired
    private AlunoRepository alunoRepository;

    @Autowired
    private UserRepository userRepository;


    public static final String ACTION_SAVE = "Salvar";

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/matriculaaluno", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        ModelAndView mv = new ModelAndView("matriculaaluno/index");
        mv.addObject("matriculasalunos", matriculaAlunoRepository.findAll());
        return mv;
    }

    @GetMapping(value = "/matriculaaluno/view/{idMatriculaAluno}")
    public ModelAndView viewPage(@PathVariable("idMatriculaAluno")MatriculaAluno matriculaAluno) {
        ModelAndView mv = new ModelAndView("matriculaaluno/VisualizarMatriculaAluno");
        mv.addObject("matriculaaluno",matriculaAluno);
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        return mv;
    }

    @RequestMapping(value = "/matriculaaluno/lista", method = RequestMethod.GET)
    public ModelAndView matriculasalunos(@ModelAttribute("current") MatriculaAluno current) {
        ModelAndView mv = new ModelAndView("matriculaaluno/listaMatriculaAluno");
        if (current != null && current.getAluno() != null && current.getAluno().getNome() != null) {
            mv.addObject("matriculasalunos", matriculaAlunoSearchRepository.findByMatriculaNomeAluno(current.getAluno().getNome()));
        }
        else {
            mv.addObject("matriculasalunos", matriculaAlunoRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/matriculaaluno/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") MatriculaAluno current) {
        ModelAndView mv = new ModelAndView("matriculaaluno/cadastrarMatriculaAluno");
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/matriculaaluno/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid @ModelAttribute("current") MatriculaAluno current, BindingResult result) {
        System.out.println(current);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        matriculaAlunoRepository.save(current);
        return new ModelAndView("redirect:/matriculaaluno/lista");
    }

    @RequestMapping(value = "/matriculaaluno/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MatriculaAluno>> getAllGrupos() {
        List<MatriculaAluno> matriculasalunos = matriculaAlunoRepository.findAll();
        return new ResponseEntity<>(matriculasalunos, HttpStatus.OK);
    }


}
