package com.apaesd.controller;

import com.apaesd.model.ResultadoAvaliacaoGeral;
import com.apaesd.repository.AlunoRepository;
import com.apaesd.repository.ResultadoAvaliacaoGeralRepository;
import com.apaesd.repository.ResultadoAvaliacaoGeralSearchRepository;
import com.apaesd.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import lombok.extern.java.Log;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Rodrigo Moliterno
 * @since 16/11/2019
 */
@Controller
@Log
public class ResultadoAvaliacaoGeralController {

    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    ResultadoAvaliacaoGeralRepository resultadoAvaliacaoGeralRepository;

    @Autowired
    ResultadoAvaliacaoGeralSearchRepository resultadoAvaliacaoGeralSearchRepository;

    @Autowired
    private UserRepository userRepository;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/resultadoavaliacaogeral", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        final ModelAndView mv = new ModelAndView("resultadoAvaliacaoGeral/index");
        mv.addObject("resultadoavaliacaogerais", resultadoAvaliacaoGeralRepository.findAll());
        return mv;
    }

    @GetMapping("/resultadoavaliacaogeral/view/{idResultadoAvaliacaoGeral}")
    public ModelAndView viewPage(@PathVariable("idResultadoAvaliacaoGeral") final ResultadoAvaliacaoGeral resultadoAvaliacaoGeral) {
        final ModelAndView mv = new ModelAndView("resultadoavaliacaogeral/VisualizarResultadoAvaliacaoGeral");
        mv.addObject("resultadoavaliacaogeral", resultadoAvaliacaoGeral);
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        return mv;
    }

    @RequestMapping(value = "/resultadoavaliacaogeral/lista", method = RequestMethod.GET)
    public ModelAndView documentos(@ModelAttribute("current") final ResultadoAvaliacaoGeral current) {
        final ModelAndView mv = new ModelAndView("resultadoavaliacaogeral/listaResultadoAvaliacaoGeral");
        if (current != null && current.getAluno() != null && current.getAluno().getNome() != null) {
            mv.addObject("avaliacaopsicologias", resultadoAvaliacaoGeralSearchRepository.findByNomeAlunoAvaliacaoGeral(current.getAluno().getNome()));

        }
        else {
            mv.addObject("resultadosavaliacaogeral", resultadoAvaliacaoGeralRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/resultadoAvaliacaoGeral/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") final ResultadoAvaliacaoGeral current) {
        final ModelAndView mv = new ModelAndView("resultadoavaliacaogeral/cadastrarResultadoAvaliacaoGeral");
        mv.addObject("tiposAlunos", alunoRepository.findAll());
        mv.addObject("tiposUsuarios", userRepository.findAll());
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/resultadoAvaliacaoGeral/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@ModelAttribute("current") final @Valid ResultadoAvaliacaoGeral current, final BindingResult result) {
        System.out.println(current);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        resultadoAvaliacaoGeralRepository.save(current);
        return new ModelAndView("redirect:/resultadoavaliacaogeral/lista");
    }


}
