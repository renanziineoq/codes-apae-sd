package com.apaesd.controller;


import com.apaesd.model.Aluno;

import com.apaesd.repository.AlunoRepository;
import com.apaesd.repository.AlunoSearchRepository;
import com.apaesd.service.ReportService;
import lombok.extern.java.Log;
import net.sf.jasperreports.engine.JRException;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author renan
 * @since
 */
@Controller
@Log
public class AlunoController {


    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    AlunoSearchRepository alunoSearchRepository;

    @Autowired
    ReportService service;

    public static final String ACTION_SAVE = "Salvar";

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/aluno", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        ModelAndView mv = new ModelAndView("aluno/index");
        mv.addObject("alunos", alunoRepository.findAll());
        return mv;
    }

    @GetMapping(value = "/aluno/view/{idAluno}")
    public ModelAndView viewPage(@PathVariable("idAluno") Aluno aluno) {
        ModelAndView mv = new ModelAndView("aluno/VisualizarAluno");
        mv.addObject("aluno", aluno);
        return mv;
    }

    @RequestMapping(value = "/aluno/lista", method = RequestMethod.GET)
        public ModelAndView alunos(@ModelAttribute("current") Aluno current) {
        ModelAndView mv = new ModelAndView("aluno/listaAluno");
        if (current != null && current.getNome() != null) {
            mv.addObject("alunos", alunoSearchRepository.findByNomeAluno(current.getNome()));
        }
       else{
            mv.addObject("alunos", alunoRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/aluno/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") Aluno current) {
        ModelAndView mv = new ModelAndView("aluno/cadastrarAluno");
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/aluno/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid @ModelAttribute("current") Aluno current, BindingResult result) {
        System.out.println(current);
        if(result.hasErrors()){
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        alunoRepository.save(current);
        return new ModelAndView("redirect:/aluno/lista");
    }

    @RequestMapping(value = "/aluno/edit", method = RequestMethod.GET)
    public ModelAndView editars(@ModelAttribute("editaluno") Aluno edit) {
        ModelAndView mv = new ModelAndView("aluno/alterarAluno");
        mv.addObject("editaluno", alunoRepository.findById(edit.getId()).orElse(null));
        mv.addObject(edit);
        return mv;
    }



    @RequestMapping(value = "/aluno/edit", method = RequestMethod.POST)
    @Transactional
    public ModelAndView saveedit(@Valid @ModelAttribute("editaluno") Aluno edit, BindingResult result) {
        System.out.println(edit);
        if(result.hasErrors()){
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return editars(edit);
        }
       alunoRepository.save(edit);
        return new ModelAndView("redirect:/aluno/lista");
    }

    @GetMapping(value = "/aluno/edit/{id}")
    public ModelAndView editar(@PathVariable("id") @ModelAttribute("id") Aluno current) {
        ModelAndView mv = new ModelAndView("aluno/alterarAluno");
        mv.addObject("editaluno", alunoRepository.findById(current.getId()).orElse(null));
        return editars(current);
    }

    @RequestMapping(value = "/aluno/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Aluno>> getAllGrupos() {
        List<Aluno>alunos = alunoRepository.findAll();
        return new ResponseEntity<>(alunos, HttpStatus.OK);
    }


    @RequestMapping(path = "/report/aluno", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public @ResponseBody byte[] generateReport() throws IOException, JRException {
        final String path = service.exportReportAluno();
        final File arquivo = new File(path);
        InputStream in = new FileInputStream(arquivo);
        return IOUtils.toByteArray(in);

    }
}
