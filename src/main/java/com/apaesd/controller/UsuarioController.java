package com.apaesd.controller;


import com.apaesd.model.Usuario;
import com.apaesd.repository.PermissaoRepository;
import com.apaesd.repository.UserRepository;
import com.apaesd.repository.UsuarioSearchRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author renan
 * @since
 */
@Controller
@Log
public class UsuarioController {


    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UsuarioSearchRepository usuarioSearchRepository;

    @Autowired
    private PermissaoRepository permissoes;

    public static final String ACTION_SAVE = "Salvar";

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    @RequestMapping(value = "/usuario", method = RequestMethod.GET)
    public ModelAndView deafultPage() {
        ModelAndView mv = new ModelAndView("usuario/index");
        mv.addObject("usuarios", userRepository.findAll());
        return mv;
    }

    @GetMapping(value = "/usuario/view/{idUsuario}")
    public ModelAndView viewPage(@PathVariable("idUsuario") Usuario usuario) {
        ModelAndView mv = new ModelAndView("usuario/VisualizarUsuario");
        mv.addObject("usuario", usuario);
        mv.addObject("tiposPermissoes", permissoes.findAll());
        return mv;
    }

    @RequestMapping(value = "/usuario/lista", method = RequestMethod.GET)
    public ModelAndView usuarios(@ModelAttribute("current") Usuario current) {
        ModelAndView mv = new ModelAndView("usuario/listaUsuario");
        if (current != null && current.getNome() != null) {
            mv.addObject("usuarios", usuarioSearchRepository.findByNomeUsuario(current.getNome()));
        } else {
            mv.addObject("usuarios", userRepository.findAll());
        }
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/usuario/add", method = RequestMethod.GET)
    public ModelAndView cadastrar(@ModelAttribute("current") Usuario current) {
        ModelAndView mv = new ModelAndView("usuario/cadastrarUsuario");
        mv.addObject("tiposPermissoes", permissoes.findAll());
        mv.addObject(current);
        return mv;
    }

    @RequestMapping(value = "/usuario/add", method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid @ModelAttribute("current") Usuario current, BindingResult result) {
        System.out.println(current);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return cadastrar(current);
        }
        current.setSenha(bCryptPasswordEncoder.encode(current.getSenha()));
        userRepository.save(current);
        return new ModelAndView("redirect:/usuario/lista");
    }

    @RequestMapping(value = "/usuario/edit", method = RequestMethod.GET)
    public ModelAndView editars(@ModelAttribute("current") Usuario edit) {
        ModelAndView mv = new ModelAndView("usuario/alterarUsuario");
        mv.addObject("edit", userRepository.findById(edit.getId()).orElse(null));
        mv.addObject("tiposPermissoes", permissoes.findAll());
        mv.addObject(edit);
        return mv;
    }

    @RequestMapping(value = "/usuario/edit", method = RequestMethod.POST)
    @Transactional
    public ModelAndView saveedit(@Valid @ModelAttribute("edit") Usuario edit, BindingResult result) {
        System.out.println(edit);
        if (result.hasErrors()) {
            log.info(String.format("Erro encontrado %s", result.getAllErrors()));
            return editars(edit);
        }
        edit.setSenha(bCryptPasswordEncoder.encode(edit.getSenha()));
        userRepository.save(edit);
        return new ModelAndView("redirect:/usuario/lista");
    }

    @GetMapping(value = "/usuario/edit/{id}")
    public ModelAndView editar(@PathVariable("id") @ModelAttribute("id") Usuario current) {
        ModelAndView mv = new ModelAndView("usuario/alterarUsuario");
        mv.addObject("edit", userRepository.findById(current.getId()).orElse(null));
        return editars(current);
    }

    @RequestMapping(value = "/usuario/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Usuario>> getAllGrupos() {
        List<Usuario> usuarios = userRepository.findAll();
        return new ResponseEntity<>(usuarios, HttpStatus.OK);
    }

    @RequestMapping(path = "/usuario/delete/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable(name = "id") Long id) {
        userRepository.deleteById(id);
        return "redirect:/usuario/lista";
    }



}
